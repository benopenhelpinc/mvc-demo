<?php

namespace Simply;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Simply\App;

class AppTest extends TestCase
{
    public function testRedirectUriWithoutSlash()
    {
        $app = new App();
        $request = new ServerRequest('GET', "/azazaz/");
        $response = $app->run($request);
        $this->assertContains('/azazaz', $response->getHeader('Location'));
        $this->assertEquals(301, $response->getStatusCode());
    }
}
