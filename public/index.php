<?php

require __DIR__ . "/../vendor/autoload.php";

$app = new \Simply\App();
$response = $app->run(\GuzzleHttp\Psr7\ServerRequest::fromGlobals());
\Http\Response\send($response);
